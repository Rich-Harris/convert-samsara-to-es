# convert-samsara-to-es

A single use project, designed to convert the [Samsara](http://samsarajs.org/) codebase from AMD to ES6 modules.

## Usage

### Clone

```bash
git clone https://gitlab.com/Rich-Harris/convert-samsara-to-es
```

### Link

```bash
cd convert-samsara-to-es
npm install
npm link
```

### Run

```bash
cd path/to/samsarajs
convert-samsara-to-es
```

## License

MIT
