#!/usr/bin/env node

var sander = require( 'sander' );
var path = require( 'path' );
var glob = require( 'glob' );
var assert = require( 'assert' );

// check we're in the right dir...
try {
	var pkg = require( path.resolve( 'package.json' ) );
	assert.equal( pkg.name, 'samsarajs' );
} catch ( err ) {
	console.log( 'Rerun convert-samsara-to-es from inside the samsarajs project directory' );
	process.exit( 1 );
}

var amdIntro = /define\(function\s*\(require, exports, module\)\s*\{/;
var amdOutro = /^\}\);/m;
var defaultExports = /module\.exports = /;
var namedExports = /exports\.\w+\s*=/;

var requireStatement = /var (\w+) = require\('(.+?)'\);/g;

function convertModule ( source ) {
	return source
		.replace( amdIntro, '' )
		.replace( amdOutro, '' )
		.replace( defaultExports, `export default ` )
		.replace( requireStatement, `import $1 from '$2.js';` )
		.replace( /^    /gm, '' );
}

function convertIndex ( source ) {
	var pattern = /(\w+): require\('(.+?)'\)/g;

	var members = [];

	while ( match = pattern.exec( source ) ) {
		members.push({ name: match[1], relativePath: match[2] });
	}

	return members
		.map( member => `export { default as ${member.name} } from '${member.relativePath}.js';` )
		.join( '\n' );
}

function convertMainIndex ( source ) {
	var pattern = /(\w+): require\('(.+?)'\)/g;

	var members = [];

	while ( match = pattern.exec( source ) ) {
		members.push({ name: match[1], relativePath: match[2] });
	}

	var imports = members
		.map( member => `import * as ${member.name} from '${member.relativePath}/index.js';` )
		.join( '\n' );

	var exports = members
		.map( member => `export * from '${member.relativePath}/index.js';` )
		.join( '\n' );

	//var namespaceExports = `export { ${ members.map( member => member.name ).join( ', ' )} };`;

	return [ imports, exports ].join( '\n\n' );
}

sander.lsrSync( 'samsara' ).forEach( file => {
	if ( path.extname( file ) !== '.js' ) return;

	var infile = path.resolve( 'samsara', file );
	var outfile = path.resolve( 'src', file );

	var source = sander.readFileSync( infile, { encoding: 'utf-8' });

	assert.ok( source.match( amdIntro ), infile );
	assert.ok( source.match( defaultExports ), infile );
	assert.ok( !source.match( namedExports ), infile );

	var converted = path.basename( file ) === 'index.js' ?
		file === 'index.js' ? convertMainIndex( source ) : convertIndex( source ) :
		convertModule( source );

	sander.writeFileSync( outfile, converted );
});
